import 'package:flutter/material.dart';

import 'package:rental/sidebar/sidebar_layout.dart';


class LoginPage extends StatelessWidget {
  double diameterKecil(BuildContext context) =>
      MediaQuery.of(context).size.width * 2 / 3;
  double diameterBesar(BuildContext context) =>
      MediaQuery.of(context).size.width * 7 / 8;
  final TextEditingController nama = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFEEEEEE),
      body: Stack(
        children: <Widget>[
          Positioned(
            right: -diameterKecil(context) / 3,
            top: -diameterKecil(context) / 3,
            child: Container(
              width: diameterKecil(context),
              height: diameterKecil(context),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                    colors: [Color(0xFF74d800), Color(0xFF6ac451)],
                    begin: Alignment.topCenter,
                  )),
            ),
          ),
          Positioned(
            left: -diameterBesar(context) / 4,
            top: -diameterBesar(context) / 4,
            child: Container(
              child: Center(
                  // child: Image(
                  //   image: AssetImage('assets/images/SewaSepeda.png'),),
                  ),
              width: diameterBesar(context),
              height: diameterBesar(context),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                    colors: [Color(0xFF00d820), Color(0xFF26bbac)],
                    begin: Alignment.topCenter,
                  )),
            ),
          ),
          Positioned(
            left: -diameterBesar(context) / 6,
            top: -diameterBesar(context) / 6,
            child: Container(
              child: Center(
                child: Image(
                  image: AssetImage('assets/images/SewaSepeda.png'),
                ),
              ),
              width: diameterBesar(context),
              height: diameterBesar(context),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                    colors: [Color(0xFF00d820), Color(0xFF26bbac)],
                    begin: Alignment.topCenter,
                  )),
            ),
          ),
          Positioned(
            right: -diameterBesar(context) / 2,
            bottom: -diameterBesar(context) / 2,
            child: Container(
                width: diameterBesar(context),
                height: diameterBesar(context),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFFc3e4de),
                )),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ListView(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  margin: EdgeInsets.fromLTRB(20, 340, 20, 10),
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 25),
                  child: Column(
                    children: <Widget>[
                      TextField(
                        decoration: InputDecoration(
                          icon: Icon(
                            Icons.people_alt_rounded,
                            color: Color(0xFF00d820),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                            color: Color(0xFF00d820),
                          )),
                          labelText: "Username:",
                          labelStyle: TextStyle(
                            color: Color(0xFF00d820),
                          ),
                        ),
                      ),
                      TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          icon: Icon(
                            Icons.vpn_key,
                            color: Color(0xFF00d820),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                            color: Color(0xFF00d820),
                          )),
                          labelText: "Password ",
                          labelStyle: TextStyle(
                            color: Color(0xFF00d820),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 20, 20),
                    child: Text(
                      "FORGOT PASSWORD?",
                      style: TextStyle(
                        color: Color(0xFF00d820),
                        fontSize: 11,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(50, 0, 50, 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.5,
                        height: 40,
                        child: Container(
                          child: Material(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.transparent,
                            child: InkWell(
                              borderRadius: BorderRadius.circular(20),
                              splashColor: Colors.amber,
                              onTap: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) {
                                      return SideBarLayout();
                                    },
                                  ),
                                );
                              },
                              child: Center(
                                child: Text(
                                  "SIGN IN",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700),
                                ),
                              ),
                            ),
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              gradient: LinearGradient(
                                colors: [Color(0xFF00d820), Color(0xFF4ED153)],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                              )),
                        ),
                      ),
                      FloatingActionButton(
                        onPressed: () {},
                        mini: true,
                        elevation: 0,
                        child: Image(
                            image: AssetImage("assets/images/google.png")),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "DON'T HAVE AN ACCOUNT?",
                      style: TextStyle(
                          fontSize: 11,
                          color: Colors.grey,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      "SIGN UP",
                      style: TextStyle(
                          fontSize: 11,
                          color: Color(0xFF00d820),
                          fontWeight: FontWeight.w700),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
