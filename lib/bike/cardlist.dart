import 'package:flutter/material.dart';
import 'package:rental/bike/bikes.dart';


class CardList extends StatelessWidget {
  final Bikes bike;
  CardList(this.bike);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Card(
        child: Row(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(top: 8, left: 8, right: 20, bottom: 8),
              child: Container(
                width: 150,
                height: 150,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/${bike.avatar}"),
                        fit: BoxFit.contain),
                    shape: BoxShape.rectangle),
                // child: RaisedButton(onPressed: (){},
                // child: Text('Sewa'),),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  bike.nama,
                  style: TextStyle(fontSize: 20, color: Colors.black),
                ),
                Row(
                  children: <Widget>[
                    // Icon(Icons.bike_scooter, color: Colors.grey),
                    // Text(bike.jenis),
                  ],
                ),
                Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    RaisedButton(
                      child: Text("Rent"),
                      onPressed: () {},
                      color: Color(0xFF429b22),
                      textColor: Colors.white,
                      // padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      // splashColor: Colors.grey,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
