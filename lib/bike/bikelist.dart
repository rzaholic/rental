import 'package:flutter/material.dart';
import 'package:rental/bike/bikes.dart';
import 'package:rental/bike/cardlist.dart';

class BikeList extends StatelessWidget {
  final bikes = [
    Bikes(nama: 'Folding Bike', avatar: 'Folding.png'),
    Bikes(nama: 'MTB', avatar: 'MTB.png'),
    Bikes(nama: 'Electric Bike', avatar: 'Electric.png'),
    Bikes(nama: 'Kids Bike', avatar: 'Kids.png'),
    Bikes(nama: 'Classic Bike', avatar: 'Vintage.png'),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: bikes.length,
        itemBuilder: (context, index) {
          return CardList(bikes[index]);
        });
  }
}
