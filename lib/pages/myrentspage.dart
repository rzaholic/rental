import 'package:flutter/material.dart';
import 'package:rental/bike/bikelist.dart';

import '../bloc.navigation_bloc/navigation_bloc.dart';

class MyRentsPage extends StatelessWidget with NavigationStates {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Choose your bike'),
          backgroundColor: Colors.white,
          centerTitle: true,
        ),
        body: BikeList());
  }
}
