import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rental/bloc.navigation_bloc/navigation_bloc.dart';



class MapPage extends StatelessWidget with NavigationStates {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
     debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: GoogleMap(
            // type dari map dengan beberapa type yakni hybrid, normal, satellite, terrain dan none
            mapType: MapType.normal,
            // posisi camera dengan menentukan lokasi berdasar latitude dan longitude
            initialCameraPosition: CameraPosition(
              target: LatLng(-8.591204, 116.116208),
              zoom: 14.4746, 
            ),
          ),
      ),
    );
  }
}