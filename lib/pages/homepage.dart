import 'package:flutter/material.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';

class HomePage extends StatelessWidget with NavigationStates {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.home),
          title: Text('Rental Sepeda'),
          actions: <Widget>[
            Icon(Icons.search),
          ],
          elevation: 0,
          backgroundColor: Colors.white,
          centerTitle: true,
        ),
        body: GridView.count(
          crossAxisCount: 2,
          mainAxisSpacing: 10.0,
          padding: EdgeInsets.only(top: 50),
          children: <Widget>[
            Column(
              children: <Widget>[
                Image(
                  height: 150,
                  width: 150.0,
                  image: AssetImage("assets/images/folding1.png"),
                ),
                Text(
                  "Folding Bike 1",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: Color(0xFF429b22),
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Image(
                  height: 150.0,
                  width: 150.0,
                  image: AssetImage("assets/images/folding2.png"),
                ),
                Text("Folding Bike 2",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Color(0xFF429b22)))
              ],
            ),
            Column(
              children: <Widget>[
                Image(
                  height: 150.0,
                  width: 150.0,
                  image: AssetImage("assets/images/folding3.png"),
                ),
                Text("Folding Bike 3",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Color(0xFF429b22)))
              ],
            ),
            Column(
              children: <Widget>[
                Image(
                  height: 150.0,
                  width: 150.0,
                  image: AssetImage("assets/images/mtb1.png"),
                ),
                Text("MTB 1",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Color(0xFF429b22)))
              ],
            ),
            Column(
              children: <Widget>[
                Image(
                  height: 150.0,
                  width: 150.0,
                  image: AssetImage("assets/images/mtb2.png"),
                ),
                Text("MTB 2",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Color(0xFF429b22)))
              ],
            ),
            Column(
              children: <Widget>[
                Image(
                  height: 150.0,
                  width: 150.0,
                  image: AssetImage("assets/images/mtb3.png"),
                ),
                Text("MTB 3",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Color(0xFF429b22)))
              ],
            ),
          ],
        ));
  }
}
